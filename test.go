package main

import (
	"io"
	"fmt"
	"os"
)

func main() {
	// Open file
	file, err := os.Open("test.go")
	// If there is a problem, throw an exception with Error err
	if err != nil {
		// Panic is like throw in Java
		panic(err)
	}
	// Defer means if the app crashes, do this thing last as a saved call
	defer func() {
		if err := file.Close(); err != nil { 
			panic(err)
		}
	}()

	buff := make([]byte, 1024)
	for {
		// Read bytes
		bytesRead, err := file.Read(buff)
		// If error with file, throw exception
		if err != nil && err != io.EOF { panic(err) }
		// If we have run out of bytes to read, break out of the for loop
		if bytesRead == 0 { break }

		// buff is in bytes, we convert bytes to string by string([]byte)
		// then print it
		fmt.Println(string(buff))
	}
}
